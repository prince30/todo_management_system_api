**Todo Management System Rest API**



- **Login API**

End Point- http://localhost:9000/todo-management-system/api/v1/auth

Method -POST

{
	"email": "test@gmail.com",
	"password": "123"
}

- **Create Todo**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks

Authorization - Bearer {token}

Method -POST

{
    "name":"new6",
    "description":"test"
}

- **Update Todo**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks/UUID-D185B1

Authorization - Bearer {token}

Method -PUT

{
    "name":"test",
    "description":"test"
}

- **Get Specific Todo**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks/UUID-D185B1

Authorization - Bearer {token}

Method -GET

- **Get All Todo**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks

Authorization - Bearer {token}

Method -GET

- **Get Specific Delete Todo**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks/UUID-D185B1

Authorization - Bearer {token}

Method -DELETE

- **Get Specific Todo Done**

End Point- http://localhost:9000/todo-management-system/api/v1/tasks/UUID-D185B1

Authorization - Bearer {token}

Method -POST






